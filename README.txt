Spree Assessment
----------------
The repo is available publicly at https://bitbucket.org/marco_theart/spree_assessment.
Anyone with a bitbucket account should be able to clone it.

It's a standard Gradle-based Android project format (without any IDE-specific files), so it can be imported by
Android Studio from the File -> New -> Import project... menu.


Running the app:
----------------
The project uses a gradle wrapper, so to execute gradle tasks, there's a gradlew (gradlew.bat for windows) shell
script in the project root.

Android Studio is normally smart enough to create a run configuration from an android module, so to run the app
from within Android Studio, you can select Run -> Run 'app' menu.
OR
From either Android Studio or a shell, install to a connected device using 'installDebug' or 'installRelease'
gradle task, depending on which variant you want to install.


Running the tests:
------------------
To run unit tests, execute the test using the 'test' gradle task from Android Studio or a shell.


The Backlog
-----------
I unfortunately didn't get around to everything I wanted to do, so the tests were the first tasks to get cut.

This is what the backlog currently looks like:
* Create instrumented test.
* Cache retrieved data locally to display while fetching fresh data.
* Implement 'load more' as only the first page of products is currently fetched from the api.
* Implement a jobqueue to handle background tasks, and notify them, or receive notifications from them via an
  eventbus (The eventbus basics is in the code, so that leaves events and event handlers)
