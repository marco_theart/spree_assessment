package com.marco.spreeassessment;

import com.marco.spreeassessment.api.dto.BrandDto;
import com.marco.spreeassessment.api.dto.PicsDto;
import com.marco.spreeassessment.api.dto.PriceDto;
import com.marco.spreeassessment.api.dto.ProductDto;
import com.marco.spreeassessment.api.dto.ProductResultSetDto;
import com.marco.spreeassessment.model.Product;
import com.marco.spreeassessment.util.DtoMapper;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Test DtoMapper.
 */
@RunWith(MockitoJUnitRunner.class)
public class DtoMapperUnitTest {

    @Test
    public void testProductLoaderWithActiveResult() {
        ProductResultSetDto exptected = createActiveTestResult();
        List<Product> underTest = DtoMapper.mapProducts(exptected.getProducts());

        Assert.assertEquals(1, underTest.size());

        Product result = underTest.get(0);
        ProductDto actual = exptected.getProducts().get(0);
        Assert.assertEquals(result.getSku(), actual.getSku());
        Assert.assertEquals(result.getTitle(), actual.getTitle());
        Assert.assertEquals(result.getPicUrl(), actual.getPics().getSmall());
        // ... etc
    }

    @Test
    public void testProductLoaderWithInactiveResult() {
        ProductResultSetDto exptected = createInactiveTestResult();
        List<Product> underTest = DtoMapper.mapProducts(exptected.getProducts());
        Assert.assertEquals(0, underTest.size());
    }

    private ProductResultSetDto createActiveTestResult() {
        ProductResultSetDto resultSet = new ProductResultSetDto();
        List<ProductDto> products = new ArrayList<>();

        ProductDto product = new ProductDto();
        product.setSku("sku");
        product.setTitle("title");
        product.setStatus(1);
        product.setIsNew(1);
        PicsDto pics = new PicsDto();
        pics.setSmall("smallPicUrl");
        product.setPics(pics);
        BrandDto brand = new BrandDto();
        brand.setName("brand");
        product.setBrand(brand);
        PriceDto price = new PriceDto();
        price.setSelling(55);
        product.setPrice(price);
        products.add(product);

        product = new ProductDto();
        product.setStatus(0);
        products.add(product);

        resultSet.setProducts(products);
        return resultSet;
    }

    private ProductResultSetDto createInactiveTestResult() {
        ProductResultSetDto resultSet = new ProductResultSetDto();
        List<ProductDto> products = new ArrayList<>();
        ProductDto product = new ProductDto();
        product.setStatus(0);
        products.add(product);
        resultSet.setProducts(products);
        return resultSet;
    }
}