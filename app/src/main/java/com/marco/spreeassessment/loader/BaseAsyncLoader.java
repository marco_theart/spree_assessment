package com.marco.spreeassessment.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

/**
 * Generic base AsyncTaskLoader
 *
 * @param <T> Data type to load
 */
public abstract class BaseAsyncLoader<T> extends AsyncTaskLoader<T> {

    T data;

    public BaseAsyncLoader(Context context) {
        super(context);
    }

    /**
     * Handles a request to start the loader.
     */
    @Override
    protected void onStartLoading() {
        if (data != null) {
            // If we currently have a result available, deliver it immediately.
            deliverResult(data);
        }
        if (takeContentChanged() || data == null) {
            // If the data has changed since the last time it was loaded or is not currently
            // available, start a load.
            forceLoad();
        }
    }

    /**
     * Handles a request to reset the loader.
     */
    @Override
    protected void onReset() {
        super.onReset();
        // Ensure the loader is stopped
        onStopLoading();
        // Release the resources associated with 'data'.
        data = null;
    }

    /**
     * Handles a request to stop the loader.
     */
    @Override
    protected void onStopLoading() {
        // Attempt to cancel the current load task if possible.
        cancelLoad();
    }

    /**
     * Called when there is new data to deliver to the client.  The super class takes care of
     * delivering it.
     *
     * @param newData The new data to deliver.
     */
    @Override
    public void deliverResult(T newData) {
        T oldData = newData;
        data = newData;
        if (isStarted()) {
            // If the loader is currently started, we can immediately deliver its results.
            super.deliverResult(newData);
        }
    }
}
