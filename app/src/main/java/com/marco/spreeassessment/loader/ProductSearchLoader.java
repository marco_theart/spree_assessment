package com.marco.spreeassessment.loader;

import android.content.Context;

import com.marco.spreeassessment.R;
import com.marco.spreeassessment.SpreeApp;
import com.marco.spreeassessment.adapter.dto.SearchResultsDto;
import com.marco.spreeassessment.api.CatalogApiService;
import com.marco.spreeassessment.api.dto.PicsDto;
import com.marco.spreeassessment.api.dto.ProductDto;
import com.marco.spreeassessment.api.dto.ProductResultSetDto;
import com.marco.spreeassessment.model.Product;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Products async loader.
 */
public class ProductSearchLoader extends BaseAsyncLoader<SearchResultsDto> {

    public static final int ID = 3;
    private final String query;

    @Inject
    public CatalogApiService catalogApiService;

    public ProductSearchLoader(Context context, String query) {
        super(context);
        this.query = query;
        ((SpreeApp) context.getApplicationContext()).getAppComponent().inject(this);
    }

    @Override
    public SearchResultsDto loadInBackground() {
        ProductResultSetDto results = catalogApiService.searchProducts(query,
                getContext().getResources().getInteger(R.integer.product_category_id));
        if (results == null || results.getProducts() == null) {
            return null;
        }
        List<Product> products = new ArrayList<>(results.getProducts().size());
        Product product;
        for (ProductDto productDto : results.getProducts()) {
            if (productDto.getStatus() == null || productDto.getStatus() == 0) {
                continue;
            }
            product = new Product();
            product.setSku(productDto.getSku());
            product.setTitle(productDto.getTitle());
            if (productDto.getPrice() != null) {
                product.setPrice(productDto.getPrice().getSelling());
            }
            if (productDto.getIsNew() != null) {
                product.setNewProduct(productDto.getIsNew() == 1);
            }
            PicsDto pics = productDto.getPics();
            if (pics != null && pics.getSmall() != null) {
                product.setPicUrl(pics.getSmall());
            }
            if (productDto.getBrand() != null) {
                product.setBrandName(productDto.getBrand().getName());
                product.setBrandLogoUrl(productDto.getBrand().getAppLogo());
            }
            products.add(product);
        }
        return new SearchResultsDto(query, products);
    }
}
