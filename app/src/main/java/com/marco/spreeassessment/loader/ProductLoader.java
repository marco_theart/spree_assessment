package com.marco.spreeassessment.loader;

import android.content.Context;

import com.marco.spreeassessment.R;
import com.marco.spreeassessment.SpreeApp;
import com.marco.spreeassessment.api.CatalogApiService;
import com.marco.spreeassessment.api.dto.ProductResultSetDto;
import com.marco.spreeassessment.model.Product;
import com.marco.spreeassessment.util.DtoMapper;

import java.util.List;

import javax.inject.Inject;

/**
 * Products async loader.
 */
public class ProductLoader extends BaseAsyncLoader<List<Product>> {

    public static final int ID = 1;

    @Inject
    public CatalogApiService catalogApiService;

    public ProductLoader(Context context) {
        super(context);
        ((SpreeApp) context.getApplicationContext()).getAppComponent().inject(this);
    }

    @Override
    public List<Product> loadInBackground() {
        ProductResultSetDto results = catalogApiService.getCatalog(getContext().getResources().getInteger(
                R.integer.product_category_id), 1);
        if (results == null || results.getProducts() == null) {
            return null;
        }
        return DtoMapper.mapProducts(results.getProducts());
    }
}
