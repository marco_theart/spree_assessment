package com.marco.spreeassessment.loader;

import android.content.Context;

import com.marco.spreeassessment.SpreeApp;
import com.marco.spreeassessment.api.CatalogApiService;
import com.marco.spreeassessment.api.dto.ProductDto;
import com.marco.spreeassessment.model.ProductDetail;
import com.marco.spreeassessment.util.DtoMapper;

import javax.inject.Inject;

/**
 * Products async loader.
 */
public class ProductDetailLoader extends BaseAsyncLoader<ProductDetail> {

    public static final int ID = 2;

    @Inject
    public CatalogApiService catalogApiService;

    private String sku;

    public ProductDetailLoader(Context context, String sku) {
        super(context);
        ((SpreeApp) context.getApplicationContext()).getAppComponent().inject(this);
        this.sku = sku;
    }

    @Override
    public ProductDetail loadInBackground() {
        ProductDto productDto = catalogApiService.getProduct(sku);
        if (productDto == null) {
            return null;
        }
        return DtoMapper.mapProduct(productDto);
    }
}
