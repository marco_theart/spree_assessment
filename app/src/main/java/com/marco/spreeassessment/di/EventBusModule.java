package com.marco.spreeassessment.di;

import com.marco.spreeassessment.pubsub.EventBus;
import com.marco.spreeassessment.pubsub.greenrobot.GreenRobotEventBus;
import com.marco.spreeassessment.pubsub.handler.CatalogHandler;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class EventBusModule {

    @Provides
    @Singleton
    EventBus provideEventBus() {
        return new GreenRobotEventBus();
    }

    @Provides
    @Singleton
    CatalogHandler provideCatalogHandler(EventBus eventBus) {
        return new CatalogHandler(eventBus);
    }
}
