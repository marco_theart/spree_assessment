package com.marco.spreeassessment.di;

import com.marco.spreeassessment.BaseActivity;
import com.marco.spreeassessment.loader.ProductDetailLoader;
import com.marco.spreeassessment.loader.ProductLoader;
import com.marco.spreeassessment.loader.ProductSearchLoader;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, ServiceModule.class, EventBusModule.class})
public interface AppComponent {

    void inject(BaseActivity activity);

    void inject(ProductLoader productLoader);

    void inject(ProductSearchLoader productSearchLoader);

    void inject(ProductDetailLoader productDetailLoader);
}
