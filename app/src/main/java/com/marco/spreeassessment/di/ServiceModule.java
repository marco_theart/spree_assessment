package com.marco.spreeassessment.di;

import android.app.Application;

import com.marco.spreeassessment.api.CatalogApiService;
import com.marco.spreeassessment.api.retrofit.RetrofitAdapterFactory;
import com.marco.spreeassessment.api.retrofit.RetrofitCatalogApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceModule {

    @Provides
    @Singleton
    CatalogApiService provideCatalogApiService(RetrofitAdapterFactory retrofitAdapterFactory) {
        return new RetrofitCatalogApiService(retrofitAdapterFactory);
    }

    @Provides
    @Singleton
    RetrofitAdapterFactory provideRetrofitAdapterFactory(Application application) {
        return new RetrofitAdapterFactory(application);
    }
}
