package com.marco.spreeassessment.model;

import java.util.List;

/**
 *
 */
public class ProductDetail extends Product {

    private List<String> galleryUrls;

    public List<String> getGalleryUrls() {
        return galleryUrls;
    }

    public void setGalleryUrls(List<String> galleryUrls) {
        this.galleryUrls = galleryUrls;
    }
}
