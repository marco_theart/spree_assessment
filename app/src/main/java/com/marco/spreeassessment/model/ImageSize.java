package com.marco.spreeassessment.model;

import android.support.annotation.StringDef;

/**
 * Annotation instead of enum. Rumour has it they are faster.
 */
@StringDef({ImageSize.SIZE_88x118, ImageSize.SIZE_206x276, ImageSize.SIZE_345x462, ImageSize.SIZE_600X803, ImageSize
        .SIZE_1200x})
public @interface ImageSize {

    String SIZE_88x118 = "88x118";
    String SIZE_206x276 = "206x276";
    String SIZE_345x462 = "345x462";
    String SIZE_600X803 = "600x803";
    String SIZE_1200x = "1200x";
}
