package com.marco.spreeassessment.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;

/**
 * Often-used Android helpers.
 */
public class AndroidUtil {

    /**
     * Checks if app instance is debuggable.
     *
     * @param context Application context.
     * @return Debuggable flag.
     */
    public static boolean isDebuggable(Context context) {
        return (context.getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE) != 0;
    }
}
