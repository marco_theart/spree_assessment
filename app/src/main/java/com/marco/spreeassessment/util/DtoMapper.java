package com.marco.spreeassessment.util;

import com.marco.spreeassessment.api.dto.PicsDto;
import com.marco.spreeassessment.api.dto.ProductDto;
import com.marco.spreeassessment.model.Product;
import com.marco.spreeassessment.model.ProductDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Dto -> Bean mapping helper.
 */
public class DtoMapper {

    public static List<Product> mapProducts(List<ProductDto> dtos) {
        if (dtos == null) {
            return null;
        }
        List<Product> products = new ArrayList<>(dtos.size());
        for (ProductDto productDto : dtos) {
            if (productDto.getStatus() == null || productDto.getStatus() == 0) {
                continue;
            }
            products.add(mapProduct(productDto));
        }
        return products;
    }

    public static ProductDetail mapProduct(ProductDto dto) {
        if (dto == null) {
            return null;
        }
        ProductDetail product = new ProductDetail();
        product.setSku(dto.getSku());
        product.setTitle(dto.getTitle());
        product.setDetail(dto.getDetail());
        product.setWhyBuy(dto.getWhyBuyPlain());
        if (dto.getPrice() != null) {
            product.setPrice(dto.getPrice().getSelling());
        }
        if (dto.getIsNew() != null) {
            product.setNewProduct(dto.getIsNew() == 1);
        }
        PicsDto pics = dto.getPics();
        if (pics != null && pics.getSmall() != null) {
            product.setPicUrl(pics.getSmall());
            if (pics.getGalleryUrls() != null) {
                product.setGalleryUrls(new ArrayList<>(pics.getGalleryUrls()));
            }
        }
        if (dto.getBrand() != null) {
            product.setBrandName(dto.getBrand().getName());
            product.setBrandLogoUrl(dto.getBrand().getAppLogo());
        }
        return product;
    }
}
