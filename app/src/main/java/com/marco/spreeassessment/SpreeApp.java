package com.marco.spreeassessment;

import android.app.Application;

import com.marco.spreeassessment.di.AppComponent;
import com.marco.spreeassessment.di.AppModule;
import com.marco.spreeassessment.di.DaggerAppComponent;

public class SpreeApp extends Application {

    private AppComponent appComponent;

    private void buildComponent() {
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        buildComponent();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
