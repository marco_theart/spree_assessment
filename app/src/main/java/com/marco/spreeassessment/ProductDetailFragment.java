package com.marco.spreeassessment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.marco.spreeassessment.adapter.ImagePagerAdapter;
import com.marco.spreeassessment.loader.ProductDetailLoader;
import com.marco.spreeassessment.model.ProductDetail;

/**
 * A fragment representing a single ProductDto detail screen.
 * This fragment is either contained in a {@link ProductListActivity}
 * in two-pane mode (on tablets) or a {@link ProductDetailActivity}
 * on handsets.
 */
public class ProductDetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<ProductDetail> {


    public static final String ARG_SKU = "sku";

    private String sku;
    private TextView productDescriptionView;
    private TextView productCareView;
    private TextView productBrand;
    private TextView productPrice;
    private ViewPager viewPager;
    private Callback callback;

    interface Callback {

        void updateImageUrl(String sku, String url);
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ProductDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(ARG_SKU)) {
            sku = getArguments().getString(ARG_SKU);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.product_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        productDescriptionView = ((TextView) view.findViewById(R.id.product_description));
        productCareView = ((TextView) view.findViewById(R.id.product_care));
        productBrand = ((TextView) view.findViewById(R.id.product_brand));
        productPrice = ((TextView) view.findViewById(R.id.product_price));
        viewPager = ((ViewPager) view.findViewById(R.id.image_pager));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Callback) {
            callback = (Callback) context;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        callback = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        getLoaderManager().restartLoader(ProductDetailLoader.ID, null, this);
    }

    private void updateTitle(String title) {
        CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) getActivity().findViewById(R.id
                .toolbar_layout);
        if (appBarLayout != null) {
            appBarLayout.setTitle(title);
        }
    }

    private void updateUI(@NonNull ProductDetail productDetail) {
        updateTitle(productDetail.getTitle());
        productDescriptionView.setText(productDetail.getWhyBuy());
        productCareView.setText(Html.fromHtml(productDetail.getDetail()));
        productBrand.setText(productDetail.getBrandName());
        productPrice.setText(getContext().getString(R.string.currency_amount, productDetail.getPrice()));
        if (callback != null) {
            callback.updateImageUrl(productDetail.getSku(), productDetail.getPicUrl());
        }
        viewPager.setAdapter(new ImagePagerAdapter(getActivity(), productDetail.getSku(), productDetail
                .getGalleryUrls()));
    }

    private void clearUI() {
        updateTitle("");
        productDescriptionView.setText("");
        productCareView.setText("");
        productBrand.setText("");
        productPrice.setText("");
    }

    @Override
    public Loader<ProductDetail> onCreateLoader(int id, Bundle args) {
        return new ProductDetailLoader(getActivity(), sku);
    }

    @Override
    public void onLoadFinished(Loader<ProductDetail> loader, ProductDetail data) {
        if (data == null) {
            clearUI();
        } else {
            updateUI(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<ProductDetail> loader) {
        clearUI();
    }
}
