package com.marco.spreeassessment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.marco.spreeassessment.pubsub.EventBus;

import javax.inject.Inject;

/**
 *
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Inject
    public EventBus eventBus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((SpreeApp) getApplication()).getAppComponent().inject(this);
    }
}
