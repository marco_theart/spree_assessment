package com.marco.spreeassessment.adapter;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Base RecyclerView adapter.
 *
 * @param <T>
 * @param <E>
 */
public abstract class GenericRecyclerViewAdapter<T, E extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<E> {

    protected List<T> data;

    public GenericRecyclerViewAdapter() {
        this(new ArrayList<T>());
    }

    public GenericRecyclerViewAdapter(List<T> data) {
        this.data = data;
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public T getItem(int position) {
        if (data == null || data.isEmpty() || position < 0 || position > getItemCount() - 1) {
            return null;
        }
        return data.get(position);
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
        notifyDataSetChanged();
    }
}
