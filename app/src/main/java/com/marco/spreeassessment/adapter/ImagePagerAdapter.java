package com.marco.spreeassessment.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.marco.spreeassessment.R;
import com.marco.spreeassessment.model.ImageSize;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 *
 */
public class ImagePagerAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;

    private String sku;
    private List<String> imageUrls;

    public ImagePagerAdapter(Context context, String sku, List<String> imageUrls) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.imageUrls = imageUrls;
        this.sku = sku;
    }

    @Override
    public int getCount() {
        return imageUrls == null ? 0 : imageUrls.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = layoutInflater.inflate(R.layout.pager_image, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.product_gallery_image);
        String url = context.getString(R.string.url_spree_image, sku, ImageSize.SIZE_206x276, imageUrls.get(position));
        container.addView(itemView);
        Picasso
                .with(context)
                .load(url)
                .placeholder(R.drawable.spree_placeholder_206x276)
                .fit()
                .centerInside()
                .error(R.drawable.spree_placeholder_206x276)
                .into(imageView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
