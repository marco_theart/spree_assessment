package com.marco.spreeassessment.adapter.dto;

import com.marco.spreeassessment.model.Product;

import java.util.List;

/**
 *
 */
public class SearchResultsDto {

    private String query;
    private List<Product> products;

    public SearchResultsDto(String query, List<Product> products) {
        this.query = query;
        this.products = products;
    }

    public String getQuery() {
        return query;
    }

    public List<Product> getProducts() {
        return products;
    }
}
