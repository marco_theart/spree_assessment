package com.marco.spreeassessment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.marco.spreeassessment.R;
import com.marco.spreeassessment.model.ImageSize;
import com.marco.spreeassessment.model.Product;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductViewAdapter extends GenericRecyclerViewAdapter<Product, RecyclerView.ViewHolder>
        implements View.OnClickListener {

    private Context context;
    private Callback callback;

    public ProductViewAdapter(Context context, List<Product> data, Callback callback) {
        super(data);
        this.context = context;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ProductViewHolder(inflater.inflate(R.layout.adapter_item_product, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ProductViewHolder productViewHolder = (ProductViewHolder) viewHolder;
        Product product = getItem(position);
        productViewHolder.itemView.setTag(position);
        productViewHolder.itemView.setOnClickListener(this);
        productViewHolder.titleView.setText(product.getTitle());
        // TODO: 2017/02/12 api has no notion of currency, so assume rands only
        productViewHolder.priceView.setText(context.getString(R.string.currency_amount, product.getPrice()));

        String thumbUrl = context.getString(R.string.url_spree_image, product.getSku(), ImageSize.SIZE_88x118, product
                .getPicUrl());
        Picasso
                .with(context)
                .load(thumbUrl)
                .placeholder(R.drawable.spree_placeholder_88x118)
                .fit()
                .centerCrop()
                .error(R.drawable.spree_placeholder_88x118)
                .into(productViewHolder.thumbnailView);
        productViewHolder.brandNameView.setText(product.getBrandName());
    }

    @Override
    public void onClick(View v) {
        if (callback == null) {
            return;
        }
        int position = Integer.valueOf(v.getTag().toString());
        if (data != null && !data.isEmpty()) {
            callback.onItemClicked(position);
        }
    }

    public interface Callback {

        void onItemClicked(int position);
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView titleView;
        TextView priceView;
        ImageView thumbnailView;
        TextView brandNameView;

        public ProductViewHolder(View itemView) {
            super(itemView);
            titleView = (TextView) this.itemView.findViewById(R.id.product_title);
            priceView = (TextView) this.itemView.findViewById(R.id.product_price);
            thumbnailView = (ImageView) this.itemView.findViewById(R.id.product_thumb);
            brandNameView = (TextView) this.itemView.findViewById(R.id.brand_name);
        }
    }
}
