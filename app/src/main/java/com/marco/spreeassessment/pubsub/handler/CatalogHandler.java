package com.marco.spreeassessment.pubsub.handler;

import com.marco.spreeassessment.pubsub.EventBus;

/**
 * Handler for catalog events.
 */
public class CatalogHandler implements EventHandler {

    private EventBus eventBus;

    public CatalogHandler(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    @Override
    public void register() {
        eventBus.register(this);
    }

    @Override
    public void unegister() {
        eventBus.unegister(this);
    }
}
