package com.marco.spreeassessment.pubsub.greenrobot;

import com.marco.spreeassessment.pubsub.Event;
import com.marco.spreeassessment.pubsub.EventBus;

/**
 * GreenRobot event bus implementation.
 */
public class GreenRobotEventBus implements EventBus {

    private final org.greenrobot.eventbus.EventBus eventBus;

    public GreenRobotEventBus() {
        eventBus = org.greenrobot.eventbus.EventBus.getDefault();
    }

    @Override
    public boolean isRegistered(Object object) {
        return eventBus.isRegistered(object);
    }

    @Override
    public void register(Object object) {
        if (!isRegistered(object)) {
            eventBus.register(object);
        }
    }

    @Override
    public void unegister(Object subscriber) {
        eventBus.unregister(subscriber);
    }

    @Override
    public void post(Event event) {
        eventBus.post(event);
    }
}
