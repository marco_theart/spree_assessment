package com.marco.spreeassessment.pubsub;

/**
 * Internal event bus contract.
 */
public interface EventBus {

    boolean isRegistered(Object object);

    void register(Object object);

    void unegister(Object subscriber);

    void post(Event event);
}
