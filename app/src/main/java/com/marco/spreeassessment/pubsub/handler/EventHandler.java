package com.marco.spreeassessment.pubsub.handler;

/**
 * Event handler. Used for events not handled in ui.
 */
public interface EventHandler {

    void register();

    void unegister();
}
