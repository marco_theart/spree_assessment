package com.marco.spreeassessment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.marco.spreeassessment.adapter.ProductViewAdapter;
import com.marco.spreeassessment.adapter.dto.SearchResultsDto;
import com.marco.spreeassessment.loader.ProductLoader;
import com.marco.spreeassessment.loader.ProductSearchLoader;
import com.marco.spreeassessment.model.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * An activity representing a list of Products. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ProductDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ProductListActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<List<Product>>,
        SearchView.OnCloseListener, SearchView.OnClickListener {

    private static final String TAG = ProductListActivity.class.getSimpleName();
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean twoPane;

    private boolean searchMode;
    private boolean searchRunning;
    private String searchQuery;

    private Fragment current;
    private ProductViewAdapter viewAdapter;
    private MenuItem searchMenu;
    private Toolbar toolbar;

    private LoaderManager.LoaderCallbacks<SearchResultsDto> searchCallbacks =
            new LoaderManager.LoaderCallbacks<SearchResultsDto>() {

                @Override
                public Loader<SearchResultsDto> onCreateLoader(int id, Bundle args) {
                    return new ProductSearchLoader(ProductListActivity.this, searchQuery);
                }

                @Override
                public void onLoadFinished(Loader<SearchResultsDto> loader, SearchResultsDto data) {
                    List<Product> results = new ArrayList<>();
                    if (data != null && data.getProducts() != null) {
                        results = data.getProducts();
                    }
                    viewAdapter.setData(results);
                    if (data != null) {
                        toolbar.setSubtitle(getString(R.string.results_for, data.getQuery()));
                    }
                }

                @Override
                public void onLoaderReset(Loader<SearchResultsDto> loader) {
                    viewAdapter.setData(new ArrayList<Product>());
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        View recyclerView = findViewById(R.id.product_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);

        if (findViewById(R.id.product_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true;
        }
        if (savedInstanceState != null) {
            searchMode = savedInstanceState.getBoolean("searchMode", false);
            searchQuery = savedInstanceState.getString("searchQuery");
            searchRunning = savedInstanceState.getBoolean("searchRunning", false);
            if (searchRunning) {
                getSupportLoaderManager().initLoader(ProductSearchLoader.ID, null, searchCallbacks);
                toolbar.setSubtitle(getString(R.string.searching_for, searchQuery));
            } else {
                getSupportLoaderManager().initLoader(ProductLoader.ID, null, this);
                toolbar.setSubtitle("");
            }
        } else {
            getSupportLoaderManager().initLoader(ProductLoader.ID, null, this);
        }
    }

    @Override
    public void onBackPressed() {
        if (searchRunning) {
            stopSearch();
            if (current != null) {
                getSupportFragmentManager().beginTransaction().remove(current).commitAllowingStateLoss();
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        /*
            Hack to determine when SearchView is closed, as bug on Android 4.0+ causes OnCloseListener
             not to be called.
        */
        searchMenu = menu.findItem(R.id.search);
        if (searchMode) {
            searchMenu.expandActionView();
        } else {
            searchMenu.collapseActionView();
        }
        MenuItemCompat.setOnActionExpandListener(searchMenu, new MenuItemCompat.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                searchMode = true;
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                searchMode = false;
                return true;
            }
        });
        final SearchView searchView = (SearchView) searchMenu.getActionView();
        if (searchMode) {
            searchView.setQuery(searchQuery, false);
        }
        searchView.setOnCloseListener(this);
        searchView.setOnSearchClickListener(this);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                searchQuery = s;
                startSearch();
                searchMenu.collapseActionView();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                searchQuery = s;
                return true;
            }
        });
        return true;
    }

    private void stopSearch() {
        searchMenu.collapseActionView();
        searchRunning = false;
        // Stop search loader
        Loader productSearchLoader = getSupportLoaderManager().getLoader(ProductSearchLoader.ID);
        if (productSearchLoader != null) {
            productSearchLoader.abandon();
        }
        // Start catalog loader
        getSupportLoaderManager().restartLoader(ProductLoader.ID, null, this);
        toolbar.setSubtitle("");
    }

    private void startSearch() {
        searchRunning = true;
        // Stop catalog loader
        Loader productLoader = getSupportLoaderManager().getLoader(ProductLoader.ID);
        if (productLoader != null) {
            productLoader.abandon();
        }
        // Start search loader
        getSupportLoaderManager().restartLoader(ProductSearchLoader.ID, null, searchCallbacks);
        toolbar.setSubtitle(getString(R.string.searching_for, searchQuery));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("searchMode", searchMode);
        outState.putString("searchQuery", searchQuery);
        outState.putBoolean("searchRunning", searchRunning);
    }

    @Override
    public boolean onClose() {
        searchMode = false;
        return false;
    }

    @Override
    public void onClick(View view) {
        searchMode = true;
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        viewAdapter = new ProductViewAdapter(ProductListActivity.this, null, new ProductViewAdapter.Callback() {

            @Override
            public void onItemClicked(int position) {
                Product product = viewAdapter.getItem(position);
                if (twoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putString(ProductDetailFragment.ARG_SKU, product.getSku());
                    current = new ProductDetailFragment();
                    current.setArguments(arguments);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.product_detail_container, current)
                            .commit();
                } else {
                    Intent intent = new Intent(ProductListActivity.this, ProductDetailActivity.class);
                    intent.putExtra(ProductDetailFragment.ARG_SKU, product.getSku());
                    startActivity(intent);
                }
            }
        });
        recyclerView.setAdapter(viewAdapter);
    }

    @Override
    public Loader<List<Product>> onCreateLoader(int id, Bundle args) {
        return new ProductLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<List<Product>> loader, List<Product> data) {
        if (data == null) {
            data = new ArrayList<>();
        }
        viewAdapter.setData(data);
    }

    @Override
    public void onLoaderReset(Loader<List<Product>> loader) {
        viewAdapter.setData(new ArrayList<Product>());
    }
}
