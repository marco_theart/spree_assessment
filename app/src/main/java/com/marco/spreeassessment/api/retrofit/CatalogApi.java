package com.marco.spreeassessment.api.retrofit;

import com.marco.spreeassessment.api.dto.ProductDto;
import com.marco.spreeassessment.api.dto.ProductResultSetDto;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Spree catalog api for Retrofit.
 */
public interface CatalogApi {

    @GET("v1/catalog/search")
    Call<ProductResultSetDto> searchProducts(@Query("q") String query, @Query("cat") int categoryId);

    @GET("v1/catalog/product/{sku}")
    Call<ProductDto> getProduct(@Path("sku") String sku);

    @GET("v1/catalog/browse/{category_id}")
    Call<ProductResultSetDto> getCatalog(@Path("category_id") int categoryId, @Query("p") int page);
}
