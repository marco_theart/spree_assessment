package com.marco.spreeassessment.api;

import com.marco.spreeassessment.api.dto.ProductDto;
import com.marco.spreeassessment.api.dto.ProductResultSetDto;

/**
 * Spree API service contract.
 */
public interface CatalogApiService {

    /**
     * ProductDto search within a category.
     *
     * @param query      String to search for.
     * @param categoryId Id of category to search within.
     * @return {@link ProductResultSetDto} of search results.
     */
    ProductResultSetDto searchProducts(String query, int categoryId);

    /**
     * Lookup product by SKU.
     *
     * @param sku SKU of product.
     * @return {@link ProductDto} if found.
     */
    ProductDto getProduct(String sku);

    /**
     * Get a paged {@link ProductResultSetDto} of products for a category.
     *
     * @param categoryId Id of category.
     * @param page       Page no.
     * @return {@link ProductResultSetDto} of results.
     */
    ProductResultSetDto getCatalog(int categoryId, int page);
}
