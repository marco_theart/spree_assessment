package com.marco.spreeassessment.api.retrofit;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Request interceptor that injects headers required by api into the request
 */
public class HeadersRequestInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request request = chain.request().newBuilder()
                .header("Accept", "application/json")
                .method(original.method(), original.body())
                .build();
        return chain.proceed(request);
    }
}
