package com.marco.spreeassessment.api.dto;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 *
 */
public class PriceDto implements Serializable {

    @SerializedName("regular")
    private Integer regular;
    @SerializedName("selling")
    private Integer selling;
    @Nullable
    @SerializedName("special_price")
    private Integer specialPrice;
    @Nullable
    @SerializedName("special_from_date")
    private Date specialFromDate;
    @Nullable
    @SerializedName("special_to_date")
    private Date specialToDate;

    public Integer getRegular() {
        return regular;
    }

    public void setRegular(Integer regular) {
        this.regular = regular;
    }

    public Integer getSelling() {
        return selling;
    }

    public void setSelling(Integer selling) {
        this.selling = selling;
    }

    @Nullable
    public Integer getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(@Nullable Integer specialPrice) {
        this.specialPrice = specialPrice;
    }

    @Nullable
    public Date getSpecialFromDate() {
        return specialFromDate;
    }

    public void setSpecialFromDate(@Nullable Date specialFromDate) {
        this.specialFromDate = specialFromDate;
    }

    @Nullable
    public Date getSpecialToDate() {
        return specialToDate;
    }

    public void setSpecialToDate(@Nullable Date specialToDate) {
        this.specialToDate = specialToDate;
    }
}
