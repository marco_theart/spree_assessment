package com.marco.spreeassessment.api.dto;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ProductDto implements Serializable {

    @SerializedName("sku")
    private String sku;
    @SerializedName("title")
    private String title;
    @SerializedName("detail")
    private String detail;
    @Nullable
    @SerializedName("why_buy_plain")
    private String whyBuyPlain;
    @Nullable
    @SerializedName("go_live")
    private Date goLive;
    @SerializedName("brand")
    private BrandDto brand;
    @SerializedName("colour")
    private ColourDto colour;
    @SerializedName("price")
    private PriceDto price;
    @SerializedName("pics")
    private PicsDto pics;
    @SerializedName("status")
    private Integer status;
    @SerializedName("visibility")
    private Integer visibility;
    @Nullable
    @SerializedName("coming_soon")
    private Integer comingSoon;
    @Nullable
    @SerializedName("editors_pick")
    private Integer editorsPick;
    @Nullable
    @SerializedName("must_have")
    private Integer mustHave;
    @Nullable
    @SerializedName("is_new")
    private Integer isNew;
    @SerializedName("show_size_chart")
    private Integer showSizeChart;
    @SerializedName("detail_url")
    private String detailUrl;
    @Nullable
    @SerializedName("simples")
    private List<ProductSimpleDto> simples;
    @Nullable
    @SerializedName("men_size_guide")
    private Integer menSizeGuide;
    @Nullable
    @SerializedName("gender")
    private String gender;
    @Nullable
    @SerializedName("department")
    private DepartmentDto department;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Nullable
    public String getWhyBuyPlain() {
        return whyBuyPlain;
    }

    public void setWhyBuyPlain(@Nullable String whyBuyPlain) {
        this.whyBuyPlain = whyBuyPlain;
    }

    @Nullable
    public Date getGoLive() {
        return goLive;
    }

    public void setGoLive(@Nullable Date goLive) {
        this.goLive = goLive;
    }

    public BrandDto getBrand() {
        return brand;
    }

    public void setBrand(BrandDto brand) {
        this.brand = brand;
    }

    public ColourDto getColour() {
        return colour;
    }

    public void setColour(ColourDto colour) {
        this.colour = colour;
    }

    public PriceDto getPrice() {
        return price;
    }

    public void setPrice(PriceDto price) {
        this.price = price;
    }

    public PicsDto getPics() {
        return pics;
    }

    public void setPics(PicsDto pics) {
        this.pics = pics;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getVisibility() {
        return visibility;
    }

    public void setVisibility(Integer visibility) {
        this.visibility = visibility;
    }

    @Nullable
    public Integer getComingSoon() {
        return comingSoon;
    }

    public void setComingSoon(@Nullable Integer comingSoon) {
        this.comingSoon = comingSoon;
    }

    @Nullable
    public Integer getEditorsPick() {
        return editorsPick;
    }

    public void setEditorsPick(@Nullable Integer editorsPick) {
        this.editorsPick = editorsPick;
    }

    @Nullable
    public Integer getMustHave() {
        return mustHave;
    }

    public void setMustHave(@Nullable Integer mustHave) {
        this.mustHave = mustHave;
    }

    @Nullable
    public Integer getIsNew() {
        return isNew;
    }

    public void setIsNew(@Nullable Integer isNew) {
        this.isNew = isNew;
    }

    public Integer getShowSizeChart() {
        return showSizeChart;
    }

    public void setShowSizeChart(Integer showSizeChart) {
        this.showSizeChart = showSizeChart;
    }

    public String getDetailUrl() {
        return detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }

    @Nullable
    public List<ProductSimpleDto> getSimples() {
        return simples;
    }

    public void setSimples(@Nullable List<ProductSimpleDto> simples) {
        this.simples = simples;
    }

    @Nullable
    public Integer getMenSizeGuide() {
        return menSizeGuide;
    }

    public void setMenSizeGuide(@Nullable Integer menSizeGuide) {
        this.menSizeGuide = menSizeGuide;
    }

    @Nullable
    public String getGender() {
        return gender;
    }

    public void setGender(@Nullable String gender) {
        this.gender = gender;
    }

    @Nullable
    public DepartmentDto getDepartment() {
        return department;
    }

    public void setDepartment(@Nullable DepartmentDto department) {
        this.department = department;
    }
}
