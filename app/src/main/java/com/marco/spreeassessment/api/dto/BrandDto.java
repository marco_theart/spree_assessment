package com.marco.spreeassessment.api.dto;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 *
 */
public class BrandDto implements Serializable {

    @SerializedName("id")
    private long id;
    @SerializedName("name")
    private String name;
    @Nullable
    @SerializedName("description")
    private String description;
    @SerializedName("url")
    private String url;
    @Nullable
    @SerializedName("mobile_image")
    private String mobileImage;
    @Nullable
    @SerializedName("app_image")
    private String appImage;
    @Nullable
    @SerializedName("app_logo")
    private String appLogo;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Nullable
    public String getMobileImage() {
        return mobileImage;
    }

    public void setMobileImage(@Nullable String mobileImage) {
        this.mobileImage = mobileImage;
    }

    @Nullable
    public String getAppImage() {
        return appImage;
    }

    public void setAppImage(@Nullable String appImage) {
        this.appImage = appImage;
    }

    @Nullable
    public String getAppLogo() {
        return appLogo;
    }

    public void setAppLogo(@Nullable String appLogo) {
        this.appLogo = appLogo;
    }
}
