package com.marco.spreeassessment.api.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 *
 */
public class ColourDto implements Serializable {

    @SerializedName("id")
    private long id;
    @SerializedName("name")
    private String name;
}
