package com.marco.spreeassessment.api.retrofit;

import android.content.Context;
import android.support.annotation.NonNull;

import com.marco.spreeassessment.util.AndroidUtil;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Factory to create and setup Retrofit adapter.
 */
public class RetrofitAdapterFactory {

    private Context context;

    /**
     * Constructor. Requires an application context for resource lookups.
     *
     * @param context Application context.
     */
    public RetrofitAdapterFactory(Context context) {
        this.context = context;
    }

    /**
     * Build Retrofit adapter from a resource id pointing to a String resource containing the url. Adds required
     * headers and full logging when in debug mode.
     *
     * @param endPointResource Id of the String resource url.
     * @return Retrofit adapter.
     */
    public Retrofit buildAdapter(int endPointResource) {
        return buildAdapter(context.getString(endPointResource));
    }

    /**
     * Build Retrofit adapter from a url. Adds required headers and full logging when in debug mode.
     *
     * @param url url
     * @return Retrofit adapter.
     */
    public Retrofit buildAdapter(@NonNull String url) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder().addInterceptor(new HeadersRequestInterceptor());
        if (AndroidUtil.isDebuggable(context)) {
            httpClient.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        }
        return buildAdapter(httpClient.build(), url);
    }

    /**
     * Helper to build Retrofit adapter from the provided OkHttpClient and a base url.
     *
     * @param client  OkHttpClient.
     * @param baseUrl Base url.
     * @return Retrofit adapter.
     */
    private Retrofit buildAdapter(OkHttpClient client, String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client).build();
    }
}
