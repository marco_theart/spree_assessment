package com.marco.spreeassessment.api.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 *
 */
public class DepartmentDto implements Serializable {

    @SerializedName("category_id")
    private Long id;
    @SerializedName("category_name")
    private String name;
}
