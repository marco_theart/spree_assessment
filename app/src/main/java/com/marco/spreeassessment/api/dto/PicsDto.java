package com.marco.spreeassessment.api.dto;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 *
 */
public class PicsDto implements Serializable {

    @Nullable
    @SerializedName("small")
    private String small;
    @Nullable
    @SerializedName("hover")
    private String hover;
    @Nullable
    @SerializedName("gallery")
    private List<String> galleryUrls;

    @Nullable
    public String getSmall() {
        return small;
    }

    public void setSmall(@Nullable String small) {
        this.small = small;
    }

    @Nullable
    public String getHover() {
        return hover;
    }

    public void setHover(@Nullable String hover) {
        this.hover = hover;
    }

    @Nullable
    public List<String> getGalleryUrls() {
        return galleryUrls;
    }

    public void setGalleryUrls(@Nullable List<String> galleryUrls) {
        this.galleryUrls = galleryUrls;
    }
}
