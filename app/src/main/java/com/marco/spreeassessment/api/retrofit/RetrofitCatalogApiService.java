package com.marco.spreeassessment.api.retrofit;

import android.util.Log;

import com.marco.spreeassessment.R;
import com.marco.spreeassessment.api.CatalogApiService;
import com.marco.spreeassessment.api.dto.ProductDto;
import com.marco.spreeassessment.api.dto.ProductResultSetDto;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.ContentValues.TAG;

/**
 * Retrofit implementation of {@link CatalogApiService}. Synchronous to allow consumer to define async strategy, ie. a
 * job queue, async tasks, threads via a thread pool executor, etc.
 */
public class RetrofitCatalogApiService implements CatalogApiService {

    private RetrofitAdapterFactory retrofitAdapterFactory;

    public RetrofitCatalogApiService(RetrofitAdapterFactory retrofitAdapterFactory) {
        this.retrofitAdapterFactory = retrofitAdapterFactory;
    }

    private CatalogApi getSpreeCatalogApi() {
        Retrofit restAdapter = retrofitAdapterFactory.buildAdapter(R.string.url_spree_catalog_api);
        return restAdapter.create(CatalogApi.class);
    }

    @Override
    public ProductResultSetDto searchProducts(String query, int categoryId) {
        ProductResultSetDto result = null;
        try {
            Response<ProductResultSetDto> response = getSpreeCatalogApi().searchProducts(query, categoryId).execute();
            if (response.isSuccessful()) {
                result = response.body();
            }
        } catch (IOException e) {
            // TODO: 2017/02/11 log and handle
            Log.e(TAG, e.getMessage(), e);
        }
        return result;
    }

    @Override
    public ProductDto getProduct(String sku) {
        ProductDto result = null;
        try {
            Response<ProductDto> response = getSpreeCatalogApi().getProduct(sku).execute();
            if (response.isSuccessful()) {
                result = response.body();
            }
        } catch (IOException e) {
            // TODO: 2017/02/11 log and handle
            Log.e(TAG, e.getMessage(), e);
        }
        return result;
    }

    @Override
    public ProductResultSetDto getCatalog(int categoryId, int page) {
        ProductResultSetDto result = null;
        try {
            Call<ProductResultSetDto> catalogCall = getSpreeCatalogApi().getCatalog(categoryId, page);
            Log.d("Request", catalogCall.request().toString());
            Response<ProductResultSetDto> response = catalogCall.execute();
            if (response.isSuccessful()) {
                result = response.body();
            }
        } catch (IOException e) {
            // TODO: 2017/02/11 log and handle
            Log.e(TAG, e.getMessage(), e);
        }
        return result;
    }
}
