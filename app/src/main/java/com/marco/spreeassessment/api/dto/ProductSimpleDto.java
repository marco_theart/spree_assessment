package com.marco.spreeassessment.api.dto;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 *
 */
public class ProductSimpleDto implements Serializable {

    @Nullable
    @SerializedName("entity_id")
    private Long entityId;
    @Nullable
    @SerializedName("size_id")
    private Long sizeId;
    @Nullable
    @SerializedName("size_value")
    private String sizeValue;
    @Nullable
    @SerializedName("stock_qty")
    private Integer stockQty;
    @Nullable
    @SerializedName("stock_status")
    private Integer stockStatus;
    @Nullable
    @SerializedName("size_order")
    private Integer sizeOrder;
}
